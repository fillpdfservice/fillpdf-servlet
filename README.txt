# Building as a JAR

To build, add a JAR artifact in your IDE (e.g. IntelliJ IDEA) and opt to include the dependencies in the output
directory and add them in the manifest classpath. Them copy them all to JavaBridge or wherever you are going to use
the class.

If you want to build an "uber-JAR," search on the Internet for how to exclude the signature files from signed JARs, or
you will have problems running it.