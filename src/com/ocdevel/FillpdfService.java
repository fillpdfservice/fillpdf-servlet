/**
 * @author renelle
 * @author wizonesolutions
 */

package com.ocdevel;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.*;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

/**
 * The below is somewhat out-of-date.
 * <p>
 * PDF::Stamper provides an interface into iText's PdfStamper allowing for the
 * editing of existing PDF's as templates. PDF::Stamper is not a PDF generator,
 * it allows you to edit existing PDF's and use them as templates.
 * <p>
 * == Creation of templates
 * <p>
 * Templates currently can only be created using Adobe LiveCycle
 * Designer which comes with the lastest versions of Adobe Acrobat
 * Professional.  Using LiveCycle Designer you can create a form and
 * add textfield's for text and button's for images.
 * <p>
 * == Example
 * <p>
 * pdf = PDF::Stamper.new("my_template.pdf")
 * pdf.text :first_name, "Jason"
 * pdf.text :last_name, "Yates"
 * pdf.image :photo, "photo.jpg"
 * pdf.save_as "my_output"
 */
public class FillpdfService {
    ByteArrayOutputStream baos = null;
    PdfStamper stamp = null;
    PdfReader reader = null;
    AcroFields form = null;

    public FillpdfService(String pdf, String type)
            throws IOException, DocumentException {
        if (type.equals("file")) {
            this.reader = new PdfReader(pdf);
        } else {
            byte[] realPdf = Base64.decodeBase64(pdf);
            this.reader = new PdfReader(realPdf);
        }
        this.baos = new ByteArrayOutputStream();
        this.stamp = new PdfStamper(this.reader, this.baos);
        this.form = this.stamp.getAcroFields();

    }

    /**
     * Takes the PDF output and sends as a string.
     */
    public String toByteArray()
            throws IOException, DocumentException {
        this.stamp.setFormFlattening(true);
        this.fill();
        return Base64.encodeBase64String(baos.toByteArray());
    }

    public String toByteArrayUnflattened()
            throws IOException, DocumentException {
        this.stamp.setFormFlattening(false);
        this.fill();
        return Base64.encodeBase64String(baos.toByteArray());
    }

    /**
     * Set a textfield defined by key and text to value.
     */
    public void text(String key, String value)
            throws IOException, DocumentException {
        try {
            this.form.setField(key, value); // Value must be a string or itext will error.
        }
        // @todo: also catch IllegalArgumentException and return the same thing
        catch (IndexOutOfBoundsException | IllegalArgumentException argException) {
            Date timestamp = new Date();
            System.err.println(timestamp.toString() + " Ran into a problem while trying to fill in a PDF field. Skipping.");
            System.err.println("This usually means that one of the values is longer than allowed.");
            System.err.println("FIELD: " + key);
            System.err.println("VALUE: " + value);
            System.err.println("Stack trace follows:");
            argException.printStackTrace(System.err);
        }
    }

    /**
     * Saves the PDF into a file defined by path given.
     */
    public void saveAs(String file)
            throws IOException, DocumentException {
        saveAs(file, true);
    }

    /**
     * Saves the PDF into a file defined by path given.
     */
    public void saveAs(String file, Boolean flatten)
            throws IOException, DocumentException {
        this.stamp.setFormFlattening(flatten);
        this.fill();
        FileOutputStream fout = new FileOutputStream(file);
        this.baos.writeTo(fout);
        fout.close();
    }

    private void fill()
            throws IOException, DocumentException {
        this.stamp.close();
        this.baos.flush();
    }


    public ArrayList<HashMap> parse() {
        ArrayList<HashMap> arr = new ArrayList();
        // Loop over the fields and get info about them
        Set<String> fields = this.form.getFields().keySet();
        for (String key : fields) {
            String type = null;
            switch (this.form.getFieldType(key)) {
                case AcroFields.FIELD_TYPE_CHECKBOX:
                    type = "Checkbox";
                    break;
                case AcroFields.FIELD_TYPE_COMBO:
                    type = "Combobox";
                    break;
                case AcroFields.FIELD_TYPE_LIST:
                    type = "List";
                    break;
                case AcroFields.FIELD_TYPE_NONE:
                    type = "None";
                    break;
                case AcroFields.FIELD_TYPE_PUSHBUTTON:
                    type = "Pushbutton";
                    break;
                case AcroFields.FIELD_TYPE_RADIOBUTTON:
                    type = "Radiobutton";
                    break;
                case AcroFields.FIELD_TYPE_SIGNATURE:
                    type = "Signature";
                    break;
                case AcroFields.FIELD_TYPE_TEXT:
                    type = "Text";
                    break;
                default:
                    type = "?";
            }
            HashMap map = new HashMap();
            map.put("name", key);
            map.put("type", type);
            map.put("value", this.form.getField(key));
            arr.add(map);
        }
        return arr;
    }

    public String parse_as_xfdf() {
        String xml = "<fields>";
        ArrayList<HashMap> arr = this.parse();
        for (HashMap map : arr) {
            xml += "\n<field name='" + map.get("name") + "' type='" + map.get("type") + "' value='" + map.get("value") + "'/>";
        }
        xml += "\n</fields>";
        return xml;
    }

    /**
     * Set a button field defined by key and replaces with an image.
     */
    public void image(String key, Image img)
            throws BadElementException, MalformedURLException, IOException, DocumentException {
        float[] img_field = this.form.getFieldPositions(key);

        Rectangle rect = new Rectangle(img_field[1], img_field[2], img_field[3], img_field[4]);
        img.scaleToFit(rect.getWidth(), rect.getHeight());
        img.setAbsolutePosition(
                (img_field[1] + (rect.getWidth() - img.getScaledWidth()) / 2),
                (img_field[2] + (rect.getHeight() - img.getScaledHeight()) / 2)
        );

        // If it is not a button, we assume it is a LiveCycle ImageField.
        if (this.form.getFieldType(key) != AcroFields.FIELD_TYPE_PUSHBUTTON) {
            PdfContentByte cb = this.stamp.getOverContent((int) img_field[0]);
            cb.addImage(img);
        }
        // Usually, it will be a button. Set its "icon" if so.
        else {
            PushbuttonField pushbuttonField = form.getNewPushbuttonFromField(key);
            pushbuttonField.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
            pushbuttonField.setProportionalIcon(true);
            pushbuttonField.setImage(img);
            this.form.replacePushbuttonField(key, pushbuttonField.getField());
        }
    }

    /**
     * @param key        Field name.
     * @param image_path File path.
     * @throws IOException
     * @throws DocumentException
     */
    public void image(String key, String image_path) throws IOException, DocumentException {
        // Idea from here http://itext.ugent.be/library/question.php?id=31
        // Thanks Bruno for letting me know about it.
        Image img;
        img = Image.getInstance(image_path);

        this.image(key, img);
    }

    public void image(String key, byte[] binaryImage) throws IOException, DocumentException {
        Image img;
        img = Image.getInstance(binaryImage);

        this.image(key, img);
    }

}
